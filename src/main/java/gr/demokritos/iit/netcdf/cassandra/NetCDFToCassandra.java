/**
 * 
 */
package gr.demokritos.iit.netcdf.cassandra;

import java.io.IOException;
import java.nio.file.Paths;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;

/**
 * @author Yiannis Mouchakis
 * <br>
 * This class is used to export NetCDF headers to Cassandra. It creates the keyspace and tables if not exist and can also 
 * export the NetCDF metadata to the keyspace.
 * The queries used by default are the following:<br>
 * 
 * <pre>
 * <code>
 * CREATE KEYSPACE IF NOT EXISTS netcdf_headers WITH replication = {'class': 'SimpleStrategy','replication_factor': '1'} ;
 * 
 * CREATE TABLE IF NOT EXISTS dimensions (
 *   dataset text,
 *   name text,
 *   is_unlimited boolean,
 *   length int,
 *   PRIMARY KEY (dataset, name)
 * );
 * 
 * CREATE TABLE IF NOT EXISTS global_attributes (
 *   dataset text,
 *   name text,
 *   type text,
 *   value text,
 *   PRIMARY KEY (dataset, name)
 * );
 * 
 * CREATE TABLE IF NOT EXISTS variable_attributes (
 *   dataset text,
 *   varname text,
 *   attrname text,
 *   attrtype text,
 *   attrvalue text,
 *   shape text,
 *   type text,
 *   PRIMARY KEY (dataset, varname, attrname)
 * );
 * <code>
 * <pre>
 * 
 */
public class NetCDFToCassandra {

	private String cassandra_address;
	private int port;

	private Cluster cluster;
    private Session session;
    
    static final String default_keyspace = "netcdf_headers";
    private String keyspace;
	private int replication_factor = 1;
	
	static final String no_attr_str = "reserved_for_null_attributes";
		
	/**
	 * 
	 * @param cassandra_address cassandra url
	 * @param port cassandra port
	 */
	public NetCDFToCassandra(String cassandra_address, int port) {

		this.cassandra_address = cassandra_address;
		this.port = port;
		this.keyspace = default_keyspace;
	}
	
	/**
	 * sets keyspace to write into. default is "netcdf_headers"
	 * @param keyspace
	 */
	public void setKeyspace(String keyspace) {
		this.keyspace = keyspace;
	}
	
	/**
	 * set replication factor. defalt is "1"
	 * @param replication_factor
	 */
	public void setReplicationFactor(int replication_factor) {
		this.replication_factor = replication_factor;
	}

	/**
	 * opens the connection to cassandra and creates keyspace and tables if not
	 * exist. must be used before export. always use close() when finished.
	 */
	public void connect() {
		cluster = Cluster.builder().addContactPoint(cassandra_address).withPort(port).build();
		session = cluster.connect();
		init();
	}

	/**
	 * closes cassandra connection
	 */
	public void close() {
		session.close();
		cluster.close();
	}

	/**
	 * creates the keyspace and tables
	 */
	private void init() {

		String create_keyspace = "CREATE KEYSPACE IF NOT EXISTS " + keyspace
				+ " WITH replication = {'class': 'SimpleStrategy',"
				+ "'replication_factor': '" + replication_factor + "'} ;";
		session.execute(create_keyspace);
		session.execute("USE " + keyspace + ";");

		String create_dims = "CREATE TABLE IF NOT EXISTS dimensions (\n"
				+ "    dataset text,\n" + "    name text,\n"
				+ "    is_unlimited boolean,\n" + "    length int,\n"
				+ "    PRIMARY KEY (dataset, name)\n" + ");";
		session.execute(create_dims);

		String create_attrs = "CREATE TABLE IF NOT EXISTS global_attributes (\n"
				+ "    dataset text,\n"
				+ "    name text,\n"
				+ "    type text,\n"
				+ "    value text,\n"
				+ "    PRIMARY KEY (dataset, name)\n" + ");";
		session.execute(create_attrs);

		String create_vars = "CREATE TABLE IF NOT EXISTS variable_attributes (\n"
				+ "    dataset text,\n"
				+ "    varname text,\n"
				+ "    attrname text,\n"
				+ "    attrtype text,\n"
				+ "    attrvalue text,\n"
				+ "    shape text,\n"
				+ "    type text,\n"
				+ "    PRIMARY KEY (dataset, varname, attrname)\n" + ");";
		session.execute(create_vars);
	}

	/**
	 * 
	 * @param netcdf_path location of the netcdf file
	 * @throws IOException when reading from the netcdf file
	 */
	public void export(String netcdf_path) throws IOException {

		NetcdfFile ncfile = NetcdfFile.open(netcdf_path);

		// dataset name is the filename
		String dataset = Paths.get(netcdf_path).getFileName().toString();

		// insert dimensions
		PreparedStatement dim_stmt = session
				.prepare("INSERT INTO dimensions (dataset, name, length, is_unlimited) VALUES "
						+ "(:d, :n, :l, :unl);");
		for (Dimension dim : ncfile.getDimensions()) {
			BoundStatement bound = dim_stmt.bind().setString("d", dataset)
					.setString("n", dim.getShortName())
					.setInt("l", dim.getLength())
					.setBool("unl", dim.isUnlimited());
			session.execute(bound);
		}

		// insert global attributes
		PreparedStatement attr_stmt = session
				.prepare("INSERT INTO global_attributes (dataset, name, value, type) VALUES "
						+ "(:d, :n, :v, :t);");
		for (Attribute attr : ncfile.getGlobalAttributes()) {
			BoundStatement bound = attr_stmt.bind().setString("d", dataset)
					.setString("n", attr.getShortName())
					.setString("v", attr.getValue(0).toString())
					.setString("t", attr.getDataType().toString());
			session.execute(bound);
		}

		// insert variables and their attributes if no attributes then this fails
		PreparedStatement var_stmt = session
				.prepare("INSERT INTO variable_attributes (dataset, varname, shape, type, attrname, attrvalue, attrtype) "
						+ "VALUES (:d, :vn, :s, :vt, :an, :av, :at);");
		for (Variable var : ncfile.getVariables()) {
			BoundStatement bound = var_stmt.bind().setString("d", dataset)
					.setString("vn", var.getShortName())
					.setString("s", var.getDimensionsString())
					.setString("vt", var.getDataType().toString());
			
			//TODO:this is a temporary fix because attrname is part of the key and cannot be null
			if (var.getAttributes().isEmpty()) {
				
				bound.setString("an", no_attr_str)
						.setString("av", no_attr_str)
						.setString("at", no_attr_str);
				session.execute(bound);
				
			} else {
				
				for (Attribute attr : var.getAttributes()) {
					bound.setString("an", attr.getShortName())
							.setString("av", attr.getValue(0).toString())
							.setString("at", attr.getDataType().toString());
					session.execute(bound);
				}
				
			}
		}

		ncfile.close();

	}
	
	

}
